package com.redbus.tictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {
	
	@Override
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_home_activity);
		setDrawerAndToolbar("Tic-Toc-Toe");

	((LinearLayout) findViewById(R.id.play)).setOnClickListener(new OnClickListener() {
		public void onClick(View V) {
			Intent intent = new Intent(HomeActivity.this, PlayActivity.class);
			startActivityForResult(intent, 0);
		}
	});


	((LinearLayout) findViewById(R.id.exit_game)).setOnClickListener(new OnClickListener() {
		public void onClick(View V) {
			Log.d("DEBUG", "Exit Game Button Pressed!");
			HomeActivity.this.finish();
		}
	});
}

	public Toolbar setDrawerAndToolbar(String name) {
		Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(appbar);
		getSupportActionBar().setTitle("");

		TextView header = (TextView) findViewById(R.id.header);
		header.setText(name);
		return appbar;
	}
}